variable "groups" {
  type = list(object({
    name        = string,
    path        = string,
    description = string,
    projects    = list(object({
      name                       = string,
      path                       = string,
      description                = string,
      kind                       = string,
      container_registry_enabled = bool,
      issues_enabled             = bool,
      wiki_enabled               = bool,
      snippets_enabled           = bool,
    }))
  }))

  default = []
}

variable unique_group_count {
  type        = number
  default     = 0
  description = "Set this variable to the count of unique groups."
}

variable parent_group_id {
  type = string
}

variable "visibility_level" {
  type        = string
  default     = "public"
  description = "The visibility level to be applied to all resources. Possible values: piublic, internal, private"
}

variable project_merge_method {
  type    = string
  default = "rebase_merge"
}

variable cicd_project_path {
  type        = string
  default     = ""
  description = "Set to a valid path value to create a CICD repo that gets added to all the other repos as a submodule."
}

variable install_cicd_deploy_key {
  type    = bool
  default = false
}

variable "cicd_deploy_key" {
  type    = string
  default = ""
}

variable "cicd_deploy_key_name" {
  type    = string
  default = ""
}
