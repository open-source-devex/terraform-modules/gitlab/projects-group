# system-cloud-structure

Terraform module for building a repository structure with groups and their respective repositories.

The input for the module is variable `groups` that takes the form of a list of groups, and groups have a list of repositories.

## Usage

See the examples directory for how to use the module.
To run the examples you need a `gitlab_token` of a user with access to the open-source-devex project, or you need to adapt the parent group ids in the examples before running them.
An easy way to provide the token is to create a _overrides_ file in the example directory:
```hcl
# secrets_override.tf
variable gitlab_token {
  default = "...."
}
```

In CI builds this value comes from a group variable.
