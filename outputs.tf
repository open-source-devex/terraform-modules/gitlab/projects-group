locals {
  map_group_id_to_path = {for group in gitlab_group.group[*] : group.id => group.path}
}

output "repository_clone_cmd" {
  value = join("\n", flatten([for group_id in keys(module.repositories.repository_clone_urls_per_group) : concat(["mkdir -p ${local.map_group_id_to_path[group_id]};", "cd ${local.map_group_id_to_path[group_id]};"], module.repositories.repository_clone_urls_per_group[group_id], ["cd ..;"])]))
}
