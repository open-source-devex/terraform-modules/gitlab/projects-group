locals {
  group_indices = range(0, length(var.groups))

  group_projects = flatten([for i in local.group_indices : [for tuple in setproduct([i], var.groups[i].projects) : {
    "${tuple[0]}" = tuple[1]
  }]])

  parent_group_ids = [for group_index in flatten([for group_project in local.group_projects : keys(group_project)]) : gitlab_group.group[tonumber(group_index)].id]
}

resource "gitlab_group" "group" {
  count = length(var.groups)

  parent_id = var.parent_group_id

  name = var.groups[count.index].name
  path = var.groups[count.index].path != "" ? var.groups[count.index].path : var.groups[count.index].name

  description = var.groups[count.index].description

  visibility_level = var.visibility_level
}

module "repositories" {
  source = "git::ssh://git@gitlab.com/open-source-devex/terraform-modules/gitlab/project.git?ref=v1.0.18"

  unique_groups = var.unique_group_count

  parent_group_ids = local.parent_group_ids

  cicd_project_path = var.cicd_project_path

  create_cicd_deploy_key = var.install_cicd_deploy_key
  cicd_deploy_key        = var.cicd_deploy_key
  cicd_deploy_key_name   = var.cicd_deploy_key_name

  projects = [for group_project in local.group_projects : {
    name                       = values(group_project)[0].name
    path                       = values(group_project)[0].path != "" ?  values(group_project)[0].path :  values(group_project)[0].name
    description                = values(group_project)[0].description != "" ? values(group_project)[0].description : "Project ${values(group_project)[0].name}"
    project_kind               = values(group_project)[0].kind
    visibility_level           = var.visibility_level
    merge_method               = var.project_merge_method
    container_registry_enabled = values(group_project)[0].container_registry_enabled
    issues_enabled             = values(group_project)[0].issues_enabled,
    wiki_enabled               = values(group_project)[0].wiki_enabled,
    snippets_enabled           = values(group_project)[0].snippets_enabled,
  }]

  default_visibility_level = var.visibility_level
  default_merge_method     = var.project_merge_method
}
