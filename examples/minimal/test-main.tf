terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-gitlab-projects-group-minimal"
    }
  }
}

variable "gitlab_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_token
}

module "test" {
  source = "../../"

  parent_group_id = "5785002" # example-implementation/testing/sub-group-1
}
