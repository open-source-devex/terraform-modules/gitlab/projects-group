terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-gitlab-projects-group-complete"
    }
  }
}

variable "gitlab_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_token
}

locals {
  parent_group = "5752175"
}

module "test" {
  source = "../../"

  parent_group_id = "5785002" # example-implementation/testing/sub-group-1

  visibility_level = "private"

  project_merge_method = "merge"

  cicd_project_path = "0-cicd"

  install_cicd_deploy_key = true
  cicd_deploy_key        = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDE5COWbGExCQGQq4v4LI4jE/iJyrwNS8qOT8NcAwoOGDlpTG6qpMhDdyduOofSUT/2r7eyfBK3UsvgnPxqcgeqYvYpVlQQPbmYZ50kgpZam/sulfnwptrHhPfm/KBdN9LzrYmpwE4gKUrvfOU7+MeYalVEQthi2viJq1SZCv7tmw=="
  cicd_deploy_key_name   = "My test deploy key"

  unique_group_count = 2

  groups = [
    {
      name        = "group1-name"
      path        = "group1-path"
      description = "Group 1 Description"
      projects    = [
        {
          name                       = "1-root"
          path                       = "1-root"
          description                = "Infrastructure root, top-level and global resources go here."
          kind                       = "terraform"
          container_registry_enabled = false,
          issues_enabled             = false,
          wiki_enabled               = false,
          snippets_enabled           = false,
        },
        {
          name                       = "2-network"
          path                       = "2-network"
          description                = "Network layer"
          kind                       = "terraform"
          container_registry_enabled = true,
          issues_enabled             = true,
          wiki_enabled               = true,
          snippets_enabled           = true,
        },
      ]
    },
    {
      name        = "group2-name"
      path        = "group2-path"
      description = "Group 2 Description"
      projects    = [
        {
          name                       = "foo"
          path                       = "bar"
          description                = "Foo Bar"
          kind                       = "packer"
          container_registry_enabled = false,
          issues_enabled             = false,
          wiki_enabled               = false,
          snippets_enabled           = false,
        },
        {
          name                       = "slug"
          path                       = ""
          description                = ""
          kind                       = "terraform"
          container_registry_enabled = true,
          issues_enabled             = true,
          wiki_enabled               = true,
          snippets_enabled           = true,
        },
      ]
    }
  ]
}

output "repository_clone_cmd" {
  value = module.test.repository_clone_cmd
}
